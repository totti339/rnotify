import os
import sys
import baker
import multiprocessing
import logging
import subprocess
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import Queue
import threading
import time

exit_flag = 0
queue_lock = threading.Lock()
queue = Queue.Queue(0)


class Worker(threading.Thread):
    def run(self):
        while not exit_flag:
            # Take lock on queue
            queue_lock.acquire()
            if not queue.empty():
                file_path = queue.get()
                # release lock and to stuff
                queue_lock.release()
                logging.debug('{} - Processing {}'.format(self.name, file_path))
                filename = os.path.basename(file_path)
                retry = 0
                result = 1
                id = filename.split(":")[1]
                while result != 0 and retry < 3:
                    logging.info('{} - Processing {}. Retry: {}'.format(self.name,file_path, retry))
                    result = subprocess.call(["./fake_script.sh", id])
                    logging.debug('{} - Job {} failed with exit code {}'.format(self.name,id, result))
                    retry += 1
                if result == 0:
                    logging.info('{} - Job {} SUCCEED'.format(self.name, id))
                    os.remove(file_path)
                else:
                    logging.warning('{} - Job {} reached max-retry. Giving up.'.format(self.name, id))
            else:
                queue_lock.release()
            time.sleep(1)


class QueueAppender(FileSystemEventHandler):
    """Custom appender to put all job in queue for multiprocessing """

    def on_created(self, event):
        queue_lock.acquire()
        queue.put(event.src_path)
        queue_lock.release()


@baker.command(default=True)
def start_notifier(path='.', max_thread=None):
    threads = []
    i = 0
    if not max_thread:
        max_thread = multiprocessing.cpu_count()
        logging.info('Starting {} thread'.format(max_thread))

    queue_appender = QueueAppender()

    # set up directory observer
    observer = Observer()
    observer.schedule(queue_appender, path=path, recursive=True)
    observer.daemon = True
    observer.start()

    while i < max_thread:
        t = Worker()
        t.daemon = True
        logging.info('Starting thread {}'.format(t.name))
        t.start()
        threads.append(t)
        i += 1

    # Infinite loop for keeping the observer up
    try:
        while True:
            time.sleep(1)
    except (KeyboardInterrupt, SystemExit):
        observer.unschedule_all()
        observer.stop()
        global exit_flag
        exit_flag = 1
        logging.info("Terminating...")

    # Wait for all threads to complete
    for t in threads:
        t.join()

    return


if __name__ == '__main__':
    # Setup Logger
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    sh = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    sh.setFormatter(formatter)
    logger.addHandler(sh)

    # Start baker
    baker.run()
